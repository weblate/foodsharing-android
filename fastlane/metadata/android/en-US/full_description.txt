With <i>Foodsharing</i> you can:

- Create and manage food baskets
- Use map functionality with Fair-Sharers and food baskets

We hope with this app you find some useful functions to make working with the foodsharing.network easier. In the future we'll constantly enhance the app to improve existing and add new functionality. For suggestions and wishes, please write us at <a href='mailto:it@foodsharing.network'>it@foodsharing.network</a>.

Should you know someone interested in supporting us with development of our app or website, also write us to <a href='mailto:it@foodsharing.network'>it@foodsharing.network</a> or visit us at <a href='https://devdocs.foodsharing.network/it-tasks.html'>devdocs.foodsharing.network/it-tasks.html</a>.

But what is foodsharing?

Since 2012 we have been saving tons of good food from the garbage every day. We distribute them voluntarily and free of charge among friends and acquaintances in the neighborhood, in homeless shelters, schools, kindergartens and through the platform foodsharing.network. Our publicly accessible shelves and refrigerators, so-called "fair dividers", are available to everyone. 200,000 people from Germany, Austria and Switzerland regularly use the Internet platform according to the motto: "Share food instead of throwing it away! Meanwhile 56,000 people also volunteer their time as food savers by using overproduced food from bakeries, Pick up and distribute to supermarkets, canteens and wholesalers. This happens continuously over 500 times a day with almost 5,500 cooperation partners.
