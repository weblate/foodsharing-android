package de.foodsharing.utils.testing

@Target(AnnotationTarget.CLASS)
annotation class OpenForTesting
