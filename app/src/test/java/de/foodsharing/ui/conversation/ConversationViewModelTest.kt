package de.foodsharing.ui.conversation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.whenever
import de.foodsharing.model.Conversation
import de.foodsharing.model.Message
import de.foodsharing.services.AuthService
import de.foodsharing.services.ConversationsService
import de.foodsharing.test.configureTestSchedulers
import de.foodsharing.test.createRandomUser
import de.foodsharing.ui.base.Event
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.subjects.PublishSubject
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import java.util.Date

class ConversationViewModelTest {

    @Mock
    lateinit var conversationsService: ConversationsService

    @Mock
    lateinit var authService: AuthService

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        configureTestSchedulers()
    }

    @Test
    fun `fetch conversation and send button`() {
        val conversationId = 1314
        val name = "convName"
        val user = createRandomUser()
        val members = listOf(user)
        val messages = listOf(Message(10, "a nice message", Date(), user))

        whenever(authService.currentUser()) doReturn Observable.just(user)

        var loadNextEvent: Observable<Any>? = null
        var errorHandler: ((Observable<Throwable>) -> ObservableSource<Any>)? = null
        val conversationSubject = PublishSubject.create<Conversation>()
        whenever(conversationsService.conversationPaged(any(), any(), any())) doAnswer {
            loadNextEvent = it.getArgument(1)
            errorHandler = it.getArgument(2)
            conversationSubject
        }

        val viewModel = ConversationViewModel(conversationsService, authService)

        viewModel.assert(
            isLoading = true,
            currentUserId = user.id
        )

        viewModel.id = conversationId

        viewModel.assert(
            isLoading = true,
            currentUserId = user.id,
            id = conversationId
        )

        val conversationDetail = Conversation(
            conversationId,
            name,
            false,
            members,
            messages.first(),
            messages
        )
        conversationSubject.onNext(conversationDetail)

        viewModel.assert(
            currentUserId = user.id,
            conversation = conversationDetail,
            id = conversationId
        )

        /*
         * Test loading the next page
         */

        var nextPageCalled = false
        loadNextEvent?.subscribe {
            nextPageCalled = true
        }
        viewModel.loadNext()

        Assert.assertEquals(true, nextPageCalled)
        viewModel.assert(
            currentUserId = user.id,
            conversation = conversationDetail,
            id = conversationId,
            isLoading = true
        )

        conversationSubject.onNext(conversationDetail)

        viewModel.assert(
            currentUserId = user.id,
            conversation = conversationDetail,
            id = conversationId
        )

        /*
         * Test send button handling
         */

        val body = "huhu"

        val sendMessageSubject = PublishSubject.create<Boolean>()
        whenever(conversationsService.send(any(), any())) doAnswer {
            Assert.assertEquals(conversationId, it.arguments[0])
            Assert.assertEquals(body, it.arguments[1])
            sendMessageSubject.ignoreElements()
        }

        viewModel.sendMessage(body)

        viewModel.assert(
            currentUserId = user.id,
            conversation = conversationDetail,
            id = conversationId,
            isSendingMessage = true
        )

        sendMessageSubject.onComplete()

        viewModel.assert(
            currentUserId = user.id,
            conversation = conversationDetail,
            id = conversationId,
            isSendingMessage = false,
            updateInput = Event("")
        )
    }

    fun ConversationViewModel.assert(
        isLoading: Boolean? = false,
        conversation: Conversation? = null,
        currentUserId: Int? = null,
        errorState: Int? = null,
        updateInput: Event<String>? = null,
        isSendingMessage: Boolean? = false,
        foodsharerId: Int? = null,
        id: Int? = null
    ) {
        Assert.assertEquals(isLoading, this.isLoading.value)
        Assert.assertEquals(conversation, this.conversation.value)
        Assert.assertEquals(currentUserId, this.currentUserId.value)
        Assert.assertEquals(errorState, this.errorState.value)
        Assert.assertEquals(updateInput?.getContentIfNotHandled(), this.updateInput.value?.getContentIfNotHandled())
        Assert.assertEquals(isSendingMessage, this.isSendingMessage.value)
        Assert.assertEquals(foodsharerId, this.foodsharerId)
        Assert.assertEquals(id, this.id)
    }
}
