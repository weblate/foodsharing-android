package de.foodsharing.api

import com.google.gson.annotations.SerializedName
import de.foodsharing.model.Basket
import de.foodsharing.model.FoodSharePoint
import io.reactivex.Observable
import retrofit2.http.GET

interface MapAPI {

    data class MapResponse(
        val status: Int,
        @SerializedName("fairteiler")
        val foodSharePoints: List<FoodSharePoint>,
        val baskets: List<Basket>
    )

    @GET("/api/map/markers?types[]=fairteiler&types[]=baskets")
    fun coordinates(): Observable<MapResponse>
}
