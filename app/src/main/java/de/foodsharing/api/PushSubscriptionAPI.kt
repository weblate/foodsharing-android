package de.foodsharing.api

import de.foodsharing.model.PushSubscription
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.POST
import retrofit2.http.Path

interface PushSubscriptionAPI {

    data class SubscriptionResponse(
        val id: Int
    )

    @POST("/api/pushnotification/android/subscription")
    fun subscribe(@Body data: PushSubscription): Single<SubscriptionResponse>

    @DELETE("/api/pushnotification/android/subscription/{id}")
    fun unsubscribe(@Path("id") id: Int): Completable
}
