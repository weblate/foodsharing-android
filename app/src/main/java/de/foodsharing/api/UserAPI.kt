package de.foodsharing.api

import de.foodsharing.model.Profile
import de.foodsharing.model.User
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Path

/**
 * Request body for a login request.
 *
 * @see UserAPI.login
 */
data class LoginRequest(val email: String, val password: String, val rememberMe: Boolean)

/**
 * Retrofit API interface for user authentication.
 */
interface UserAPI {
    /**
     * Fetches the current logged in user, or 401.
     * @return the user's id and name
     */
    @GET("/api/user/current")
    fun currentUser(): Observable<User>

    /**
     * Fetches the profile of the currently logged in user
     */
    @GET("/api/user/current/details")
    fun currentUserDetails(@Header("Cache-Control") cacheControl: String): Observable<Profile>

    /**
     * Fetches the user information of the user with the given id.
     */
    @GET("/api/user/{id}")
    fun getUser(@Path("id") id: Int): Observable<User>

    /**
     * Logs in the user.
     * @param data email and password
     * @return the user's id and name
     */
    @POST("/api/user/login")
    fun login(@Body data: LoginRequest): Observable<User>

    /**
     * Logs out the user.
     */
    @POST("/api/user/logout")
    fun logout(): Observable<Unit>
}
