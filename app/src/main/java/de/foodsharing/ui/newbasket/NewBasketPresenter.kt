package de.foodsharing.ui.newbasket

import de.foodsharing.model.Basket
import de.foodsharing.model.Coordinate
import de.foodsharing.services.BasketService
import de.foodsharing.services.PreferenceManager
import de.foodsharing.services.ProfileService
import de.foodsharing.ui.base.BasePresenter
import java.io.File
import javax.inject.Inject

class NewBasketPresenter @Inject constructor(
    private val baskets: BasketService,
    private val profile: ProfileService,
    private val preferenceManager: PreferenceManager
) : BasePresenter<NewBasketContract.View>(), NewBasketContract.Presenter {

    override fun fetch() {
        request(profile.current(), { profile ->
            view?.showDefaults(
                profile, preferenceManager.wasLastContactByMessage,
                preferenceManager.wasLastContactByPhone
            )
        }, {
            // Ignore for now and pass null
            view?.showDefaults(
                null, preferenceManager.wasLastContactByMessage,
                preferenceManager.wasLastContactByPhone
            )
        })
    }

    override fun publish(
        description: String,
        phone: String?,
        mobile: String?,
        contactByMessage: Boolean,
        lifetime: Int,
        location: Coordinate?,
        picture: File?
    ) {
        val contactTypes = ArrayList<Int>()
        val contactByPhone = phone != null && mobile != null

        if (contactByPhone) contactTypes.add(Basket.CONTACT_TYPE_PHONE)
        if (contactByMessage) contactTypes.add(Basket.CONTACT_TYPE_MESSAGE)

        preferenceManager.wasLastContactByMessage = contactByMessage
        preferenceManager.wasLastContactByPhone = contactByPhone

        request(
                baskets.create(
                        description,
                        contactTypes.toTypedArray(),
                        phone,
                        mobile,
                        lifetime,
                        location?.lat,
                        location?.lon
                ), {
            val basket = it.basket!!

            if (picture != null) {
                request(baskets.setPicture(basket.id, picture), {
                    view?.display(it.basket!!)
                }, {
                    it.localizedMessage?.let { m ->
                        view?.showError(m)
                    }
                    view?.display(basket)
                })
            } else view?.display(basket)
        }, {
            it.localizedMessage?.let { m ->
                view?.showError(m)
            }
        })
    }
}
