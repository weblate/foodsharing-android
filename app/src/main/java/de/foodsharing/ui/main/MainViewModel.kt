package de.foodsharing.ui.main

import androidx.lifecycle.MutableLiveData
import de.foodsharing.api.PopupAPI
import de.foodsharing.model.Popup
import de.foodsharing.services.AuthService
import de.foodsharing.services.PreferenceManager
import de.foodsharing.ui.base.BaseViewModel
import de.foodsharing.ui.base.Event
import de.foodsharing.utils.POPUP_CONFIG_ENDPOINT
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val auth: AuthService,
    private val popupAPI: PopupAPI,
    private val preferenceManager: PreferenceManager
) : BaseViewModel() {
    val currentUserName = MutableLiveData<String>().apply { value = "" }
    val currentUserId = MutableLiveData<Int>().apply { value = -1 }

    val popup = MutableLiveData<Event<List<Popup>>>()

    init {
        request(auth.currentUser(), {
            currentUserName.value = it.name
            currentUserId.value = it.id
        }, {
            // Ignore errors for now
        })

        request(popupAPI.current(POPUP_CONFIG_ENDPOINT), {
            popup.postValue(Event(it))
        }, {
            // Ignore error, popup will be shown on next launch
        })
    }

    fun logout() {
        // Do not autoDispose this call, to allow to finish the logout after the MainActivity closes
        preferenceManager.pushNotificationsEnabled = false
        request(auth.logout(), onError = {})
    }
}
