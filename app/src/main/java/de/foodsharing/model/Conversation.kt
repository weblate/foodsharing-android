package de.foodsharing.model

/**
 * A conversation in the chat between multiple users.
 */
data class Conversation(
    val id: Int,
    val title: String?,
    val hasUnreadMessages: Boolean,
    val members: List<User>,
    val lastMessage: Message?,
    val messages: List<Message>
)
