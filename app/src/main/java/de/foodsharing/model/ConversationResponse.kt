package de.foodsharing.model

import com.google.gson.annotations.SerializedName
import de.foodsharing.utils.MissingProfileException
import java.util.Date

data class ConversationListResponse(
    val conversations: List<ConversationResponse>,
    val profiles: List<User>
)

data class ConversationDetailResponse(
    val conversation: ConversationResponse,
    val profiles: List<User>
)

data class ConversationIdResponse(
    val id: Int
)

data class ConversationMessagesResponse(
    val messages: List<MessageResponse>,
    val profiles: List<User>
)

data class ConversationResponse(
    val id: Int,
    val title: String?,
    @SerializedName("hasUnreadMessages")
    val hasUnreadMessages: Boolean,
    val members: List<Int>,
    @SerializedName("lastMessage")
    val lastMessage: MessageResponse?,
    val messages: List<MessageResponse>?
) {
    fun toConversation(profiles: Map<Int, User>): Conversation {
        return Conversation(
            id = id,
            title = title,
            hasUnreadMessages = hasUnreadMessages,
            members = members.map { profiles.getByIdOrThrow(it) },
            lastMessage = lastMessage?.toMessage(profiles),
            messages = messages?.map { it.toMessage(profiles) } ?: emptyList()
        )
    }
}

data class MessageResponse(
    val id: Int,
    val body: String,
    @SerializedName("sentAt")
    val sentAt: Date,
    @SerializedName("authorId")
    val authorId: Int
) {
    fun toMessage(profiles: Map<Int, User>): Message {
        return Message(
            id = id,
            body = body,
            sentAt = sentAt,
            author = profiles.getByIdOrThrow(authorId)
        )
    }
}

private fun Map<Int, User>.getByIdOrThrow(id: Int): User {
    return this[id] ?: throw MissingProfileException(id)
}
