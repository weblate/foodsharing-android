package de.foodsharing.di

import dagger.Module

@Module
abstract class FlavorActivitiesModule {
    // Allow subtypes to add module specific components
}
